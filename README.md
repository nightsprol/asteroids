# Readme

This is a GMS2 project based on Atari's game Asteroids. It is intended for educational purposes.

## Gameplay

It more or less follows the basic rules of the original Asteroids game.

![Screenshot](asteroids.png "Screenshot")

- There is a ship controlled by the player that can turn (left/right arrows), move forwards (up arrow), fire lasers (space), and jump to "hyperspace" (shift).
- There are 3 sizes of asteroid, with each subsequent size half of the previous.
- The ship, asteroids, and lasers all wrap around the edges of the screen.
- When an asteroid is destroyed, it splits into two asteroids of the smaller size. If it is the smallest size, it disappears.
- When all the asteroids are destroyed, the next level begins with 1 more astroid than before.
- Destroying large astroids provides 20 points, medium 50, and small 100.
- When the ship collides with an asteroid, the player loses a life and is reset to the starting position.
- The player begins with 3 extra lives (so 4 total when including the first life).
- A game over screen is shown once the player runs out of lives, and the player can press "R" to restart.

## Project Layout

### Objects
There are 3 primary objects in the game:

- `o_ship`
  - Controlled with left, right, up, and space keys to turn, move, and fire respectively.
  - Movement uses the builtin variables `direction` and `speed`, and the `image_angle` is set to `direction` to always point the same way.
  - Tracks the score, lives, and game over state.
  - Has collision code with the asteroids that handles losing lives and game over.
- `o_asteroid`
  - On create, selects a random frame of the image and moves in a random direction, with some a random speed (within a range).
  - On collision with a laser, checks which asteroid sprite is in use (large, medium, small) to decide what score to give and whether to spawn more asteroids or not.
  - Note that there are 3 different sprites used by this, `s_asteroid_large` and similar names for medium and small.
- `o_laser`
  - On create, moves in the same `direction` as `o_ship` with a constant `speed`, and sets an alarm with 1 second.
  - When the alarm fires, the laser is destroyed.

There are a couple additional objects that are very simple:

- `o_wrapping_parent`
  - This is a common parent of all interactable objects in the game: `o_ship`, `o_asteroid`, and `o_laser`.
  - The only event it has is a Begin Step event that just calls `move_wrap()` to enable screen wrapping.
- `o_explosion`
  - This is used as an animation when asteroids or the ship explode.
  - The only event it has is Animated End, where the object is destroyed.

### Sprites
Some quick notes about the sprites:

- All of them have their origin set to Middle Center
- The `s_asteroid_*` sprites have their FPS set to 0, since they are not animated and just picked a random frame to show on Create.
- The `s_asteroid_*` sprites have their Collision Mask set to Automatic and Precise Per Frame.
- `s_explosion` has its FPS set to 8.
- `s_ship` has its FPS set to 6. Note that `o_ship` has some logic to set its `image_speed` to 1/0 depending on whether it is moving forwards or not.

### Other Assets / Settings
There are some sound effects and fonts I added as well.

The game should also have Interpolation disabled in the graphics settings.

## Teaching
Some thoughts on what new concepts/mechanics are present:

- All interactable objects use `direction` and `speed` for movement instead of `hspeed`/`vspeed`.
- This game has sound effects that are triggered in various events: firing, asteroids exploding, ship exploding, level up.
- There is a parent object that is used to enable the same screen wrapping behavior in all interactable objects.
- Asteroids reference both their frame (`image_index`) and sprite (`sprite_index`).
- Asteroids use the `var` keyword to set a builtin variable on a specific object returned by `instance_create_depth()`.
- There are multiple uses of random functions: `random()`, `irandom()`, and `random_range()`.

Additionally, there are quite a few improvements that can be made which are mentioned below. It may work well to give some basic part of this game (at least the sprite and sound assets), and then have the class build on top of it. The game could also be reskinned to have a completely different theme instead of matching the retro space them of Asteroids.

Settings the class could tweak to their liking:
- Speed of the ship, asteroids, and lasers.
- Turn speed of the ship.
- Number of lives that the player starts with.
- How many points the player gets for destroying each asteroid.
- Number of asteroids that spawn when a larger asteroid is destroyed.
- Number of asteroids that spawn on the first level.
- Number of asteroids that are added on each level.
- Difficulty ramping: some of the above could change with the level to make the game harder.

## Future Improvements

- The player can fire as fast as they can press the fire button. A cooldown could be added, and the control could be changed to allowing the button to be held down.
- There are no UFOs like in the original Asteroids game, and there are no "storms" (or whatever they are called).
- The player does not gain an extra life every 10,000 points like the original game.
- There is no high score table after the Game Over screen.
- There is no delay between levels.
- On level up, new asteroids are always created in the top-left corner. They could be created in random positions instead, while avoiding the player.
- A gamepad (eg. Xbox controller) could be used instead of the keyboard. There are some comments on how to do this.
- When the ship explodes, it resets to the middle of the screen. If an asteroid is there, all of the player's lives will instantly be used up.
- The ship has instantaneous velocity for both starting and stopping. It should have momentum using acceleration and friction.
