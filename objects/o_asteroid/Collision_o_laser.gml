instance_destroy(other);

if sprite_index == s_asteroid_large {
	o_ship.player_score += 20;
	
	repeat (2) {
		var asteroid = instance_create_depth(x, y, 0, o_asteroid);
		asteroid.sprite_index = s_asteroid_medium;
	}
} else if sprite_index == s_asteroid_medium {
	o_ship.player_score += 50;
	
	repeat (2) {
		var asteroid = instance_create_depth(x, y, 0, o_asteroid);
		asteroid.sprite_index = s_asteroid_small;
	}
} else {
	o_ship.player_score += 100;
}

audio_play_sound(snd_asteroid, 0, false);
instance_create_depth(x, y, 0, o_explosion);

instance_destroy();