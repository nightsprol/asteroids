if game_over == false {
	audio_play_sound(snd_explosion, 0, false);
	instance_create_depth(x, y, 0, o_explosion);
	
	if player_lives == 0 {
		image_alpha = 0;
		game_over = true;
	} else {
		player_lives -= 1;

		// Reset player back to the start
		// TODO: An improvement would be to prevent the 
		// player from respawning on an asteroid.
		x = xstart;
		y = ystart;
	}
}