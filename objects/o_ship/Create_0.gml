image_speed = 0;

// Movement and turning
MOVE_SPEED = 3;
TURN_SPEED = 3;
direction = 90;

// Lives and score
player_lives = 3;
player_score = 0;
game_over = false;

// Game level, determines how many asteroids there should be.
game_level = instance_number(o_asteroid);