draw_set_font(f_interface);
draw_set_valign(fa_top);
draw_set_halign(fa_left);
draw_text(10, 10, "Score: " + string(player_score));
draw_text(10, 35, "Lives: " + string(player_lives));

if game_over == true {
	draw_set_font(f_game_over);
	draw_set_valign(fa_middle);
	draw_set_halign(fa_center);
	
	draw_text(room_width / 2, room_height / 2, "GAME OVER");
	
	draw_set_font(f_interface);
	draw_text(room_width / 2, (room_height / 2) + 60, "Press R to restart");
}

// TODO: We could show a high score table