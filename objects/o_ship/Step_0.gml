// Restarting the game.
if keyboard_check_pressed(ord("R")) {
	game_restart();
}

// Disabling the ship's logic if the game is over.
if game_over == true {
	exit;
}

// We could use gamepad inputs instead of the keyboard to change it up:
//	Left:		gamepad_axis_value(0, gp_axislh) < -0.5		// Left stick, moved left
//	Right:		gamepad_axis_value(0, gp_axislh) > 0.5		// Left stick, moved right
//	Forward:	gamepad_button_check(0, gp_face3)			// X on Xbox controller
//	Fire:		gamepad_button_check(0, gp_face1)			// A on Xbox controller
// Or use the gamepad's left and right triggers for forward and fire.

// Moving and turning.
if keyboard_check(vk_left) {
	direction += TURN_SPEED;
}

if keyboard_check(vk_right) {
	direction -= TURN_SPEED;
}

// Point the ship's sprite in the same direction that it is moving.
image_angle = direction;

// TODO: The ship should have momentum with acceleration and friction.
if keyboard_check(vk_up) {
	image_speed = 1;
	speed = MOVE_SPEED;
} else {
	image_speed = 0;
	speed = 0;
}

// Going into "hyperspace": teleporting to a random location in the room.
if keyboard_check_pressed(vk_shift) {
	x = random(room_width);
	y = random(room_height);
}

// Firing the laser.
if keyboard_check_pressed(vk_space) {
	instance_create_depth(x, y, 0, o_laser);
}

// Increasing the level after the level was cleared.
if instance_number(o_asteroid) == 0 {
	game_level += 1;
	
	repeat (game_level) {
		// TODO: We could spawn the asteroids in random locations,
		// but we would need to avoid spawning them on top of the player.
		// Spawning in the top left corner is the simpler approach for now.
		instance_create_depth(0, 0, 0, o_asteroid);
	}
	
	audio_play_sound(snd_level_up, 0, false);
	
	x = xstart;
	y = ystart;
}